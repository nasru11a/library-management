# Read Me First
### 1. docker-compose.yaml
I have set up a database in mysql docker image. 
In order to run the application first run docker-compose up

### 2. run the application
Now you can run the application.

### 3. import postman collection
In order to validate the api requirements import
postman collection file into your local postman app.

### 4. APIs
#### In folder auth there are 2 APIs
*    1. sign-up
*    2. sign-in
#### In folder books there are 4 APIs
*    1. publish a book
*    2. list all books
*    3. search for book
*    4. update 
