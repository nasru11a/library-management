package az.librarymanagement.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = Book.TABLE_NAME)
public class Book {
    public static final String TABLE_NAME = "books";
    private static final long serialVersionUID = -345588320444257803L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(nullable = false, name = "user_id")
    User user;

    @Column(nullable = false)
    String name;

    @Column(nullable = false)
    String author;

    String details;
}
