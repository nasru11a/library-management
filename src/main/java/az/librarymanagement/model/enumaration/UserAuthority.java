package az.librarymanagement.model.enumaration;

public enum UserAuthority {
    ROLE_USER, ROLE_PUBLISHER, ROLE_ADMIN, ROLE_SUPER_USER
}
