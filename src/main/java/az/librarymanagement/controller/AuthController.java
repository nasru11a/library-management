package az.librarymanagement.controller;

import az.librarymanagement.dto.SignInDto;
import az.librarymanagement.dto.SignUpDto;
import az.librarymanagement.service.AuthService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/auth")
public class AuthController {

    private final AuthService userService;

    @PostMapping("/sign-up")
    public ResponseEntity<Void> signUp(@RequestBody @Valid SignUpDto dto){
        log.info("Sign up request with email {}", dto.getEmail());
        userService.signUp(dto);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/sign-in")
    public ResponseEntity<Void> signIn(@RequestBody @Valid SignInDto dto) {
        log.info("Sign in request with email {}", dto.getEmail());
        userService.signIn(dto);
        return ResponseEntity.ok().build();
    }


}
