package az.librarymanagement.controller;

import az.librarymanagement.dto.BookRequestDto;
import az.librarymanagement.dto.BookResponseDto;
import az.librarymanagement.dto.BookUpdateDto;
import az.librarymanagement.service.BookService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/books")
public class BookController {

    private final BookService bookService;

    @PostMapping("/publish")
    public String publishNewBook(@RequestBody @Valid BookRequestDto dto, Principal principal) {
        log.info("Publish a new book request with name: {}", dto.getName());
        return bookService.publishNewBook(principal.getName(), dto);
    }

    @GetMapping("/list")
    public ResponseEntity<List<BookResponseDto>> listAllBooks() {
        log.info("List all books request");
        return new ResponseEntity<>(bookService.listAllBooks(), HttpStatus.OK);
    }

    @PutMapping("/update")
    public ResponseEntity<BookResponseDto> updateBook(@RequestParam Long bookId,
                                                      @RequestBody @Valid BookUpdateDto dto,
                                                      Principal principal){
        log.info("Update book request for book: {}", dto.getName());
        return new ResponseEntity<>(bookService.updateBook(bookId, dto, principal.getName()), HttpStatus.OK);
    }

    @GetMapping("/search")
    public ResponseEntity<Page<BookResponseDto>> search(@RequestParam(required = false, defaultValue = "") String name,
                                                        @RequestParam(required = false, defaultValue = "") Long publisherId,
                                                        Pageable pageable) {
        log.info("Search book request with name: {}", name);
        return ResponseEntity.ok(bookService.search(name, publisherId, pageable));
    }
}
