package az.librarymanagement.errors;

import az.librarymanagement.exception.ErrorResponse;
import org.springframework.http.HttpStatus;

public enum Errors implements ErrorResponse {

    EMAIL_ALREADY_REGISTERED("EMAIL_ALREADY_REGISTERED", HttpStatus.BAD_REQUEST,
            "Given email {email} is already registered."),
    USER_NOT_FOUND("USER_NOT_FOUND", HttpStatus.BAD_REQUEST,
            "Given user is not found in the system."),
    INVALID_PASSWORD("INVALID_PASSWORD", HttpStatus.BAD_REQUEST,
            "Password is invalid."),
    USER_IS_NOT_PUBLISHER("USER_IS_NOT_PUBLISHER", HttpStatus.BAD_REQUEST,
            "User is not a publisher."),
    BOOK_IS_ALREADY_PUBLISHED("BOOK_IS_ALREADY_PUBLISHED", HttpStatus.BAD_REQUEST,
            "Given book \"{book}\" is already published."),
    PUBLISHER_NOT_FOUND("PUBLISHER_NOT_FOUND", HttpStatus.BAD_REQUEST,
            "Publisher is not found."),
    BOOK_NOT_FOUND("BOOK_NOT_FOUND", HttpStatus.BAD_REQUEST,
            "Given book is not found in the library."),
    INVALID_PUBLISHER_AUTHORITY("INVALID_PUBLISHER_AUTHORITY", HttpStatus.BAD_REQUEST,
            "You cannot update this book because of your privileges.");

    String key;
    HttpStatus httpStatus;
    String message;

    Errors(String key, HttpStatus httpStatus, String message) {
        this.message = message;
        this.key = key;
        this.httpStatus = httpStatus;
    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public HttpStatus getHttpStatus() {
        return httpStatus;
    }
}
