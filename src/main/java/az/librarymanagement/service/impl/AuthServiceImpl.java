package az.librarymanagement.service.impl;

import az.librarymanagement.dto.SignInDto;
import az.librarymanagement.dto.SignUpDto;
import az.librarymanagement.errors.Errors;
import az.librarymanagement.exception.ApplicationException;
import az.librarymanagement.model.Authority;
import az.librarymanagement.model.User;
import az.librarymanagement.model.enumaration.UserAuthority;
import az.librarymanagement.repository.AuthorityRepository;
import az.librarymanagement.repository.UserRepository;

import az.librarymanagement.service.AuthService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Slf4j
@Service
@RequiredArgsConstructor
public class AuthServiceImpl implements AuthService {

    private final UserRepository userRepository;
    private final AuthorityRepository authorityRepository;
    private final PasswordEncoder passwordEncoder;

    @Override
    public void signUp(SignUpDto dto) {
        log.trace("Sign up request with email {}", dto.getEmail());
        userRepository.findByUsername(dto.getEmail())
                .ifPresent(user -> {
                    throw new ApplicationException(Errors.EMAIL_ALREADY_REGISTERED,
                            Map.of("email", dto.getEmail()));});

        User user = createUserEntityObject(dto);
        assignAuthoritiesToUser(user, dto);
        userRepository.save(user);
    }

    @Override
    public void signIn(SignInDto dto) {
        log.trace("Sign in request with email {}", dto.getEmail());
        User user = userRepository.findByUsername(dto.getEmail())
                .orElseThrow(() -> new ApplicationException(Errors.USER_NOT_FOUND));
        matchPassword(user, dto);
    }

    public void matchPassword(User user, SignInDto dto){
        boolean isMatched = passwordEncoder.matches(dto.getPassword(), user.getPassword());
        if(!isMatched) throw new ApplicationException(Errors.INVALID_PASSWORD);
    }

    private User createUserEntityObject(SignUpDto dto) {
        return User.builder()
                .username(dto.getEmail())
                .firstName(dto.getFirstName())
                .lastName(dto.getLastName())
                .phone(dto.getPhone())
                .email(dto.getEmail())
                .password(passwordEncoder.encode(dto.getPassword()))
                .accountNonExpired(true)
                .accountNonLocked(true)
                .credentialsNonExpired(true)
                .enabled(true)
                .build();
    }

    private void assignAuthoritiesToUser(User user, SignUpDto dto) {
        Set<Authority> authorities = new HashSet<>();
        if (dto.isIAmPublisher()) {
            authorities.add(authorityRepository.findByAuthority(UserAuthority.ROLE_PUBLISHER.name()));
            authorities.add(authorityRepository.findByAuthority(UserAuthority.ROLE_USER.name()));
        } else {
            authorities.add(authorityRepository.findByAuthority(UserAuthority.ROLE_USER.name()));
        }
        user.setAuthorities(authorities);
    }
}
