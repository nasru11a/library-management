package az.librarymanagement.service.impl;

import az.librarymanagement.dto.BookRequestDto;
import az.librarymanagement.dto.BookResponseDto;
import az.librarymanagement.dto.BookUpdateDto;
import az.librarymanagement.errors.Errors;
import az.librarymanagement.exception.ApplicationException;
import az.librarymanagement.model.Book;
import az.librarymanagement.model.User;
import az.librarymanagement.model.enumaration.UserAuthority;
import az.librarymanagement.model.specs.BookSpecification;
import az.librarymanagement.model.specs.SearchCriteria;
import az.librarymanagement.model.specs.SearchOperation;
import az.librarymanagement.repository.BookRepository;
import az.librarymanagement.repository.UserRepository;
import az.librarymanagement.service.BookService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class BookServiceImpl implements BookService {

    private final BookRepository bookRepository;
    private final UserRepository userRepository;
    private final ModelMapper mapper;

    @Override
    public String publishNewBook(String username, BookRequestDto dto) {
        errorIfUserDoesNotHavePublisherAuthority();
        User user = userRepository.findByUsername(username)
                .orElseThrow(() -> new ApplicationException(Errors.USER_NOT_FOUND));

        bookRepository.findByNameAndAuthor(dto.getName(), dto.getAuthor())
                .ifPresent(book -> {
                    throw new ApplicationException(Errors.BOOK_IS_ALREADY_PUBLISHED,
                            Map.of("book", dto.getName()));
                });

        Book book = Book.builder()
                .user(user)
                .name(dto.getName())
                .author(dto.getAuthor())
                .details(dto.getDetails())
                .build();
        bookRepository.save(book);
        return String.format("Book with name \"%s\" is published by %s.", dto.getName(), user.getFirstName());
    }

    @Override
    public List<BookResponseDto> listAllBooks() {
        return bookRepository.findAll()
                .stream()
                .map(book -> mapper.map(book, BookResponseDto.class))
                .collect(Collectors.toList());
    }

    @Override
    public BookResponseDto updateBook(Long bookId, BookUpdateDto dto, String username) {
        User user = userRepository.findByUsername(username)
                .orElseThrow(()-> new ApplicationException(Errors.USER_NOT_FOUND));
        Book book = bookRepository.findById(bookId)
                .orElseThrow(() -> new ApplicationException(Errors.BOOK_NOT_FOUND));

        if (!(Objects.equals(user.getId(), book.getUser().getId()))) {
            throw new ApplicationException(Errors.INVALID_PUBLISHER_AUTHORITY);
        }

        if (!(dto.getName() == null)) {
            book.setName(dto.getName());
        } else if (!(dto.getAuthor() == null)) {
            book.setAuthor(dto.getAuthor());
        } else if (!(dto.getDetails() == null)) {
            book.setDetails(dto.getDetails());
        }

        bookRepository.save(book);
        return mapper.map(book, BookResponseDto.class);
    }

    @Override
    public Page<BookResponseDto> search(String name, Long publisherId, Pageable pageable) {
        BookSpecification specification = new BookSpecification();

        if (publisherId != null) {
            User user = userRepository.findById(publisherId)
                    .orElseThrow(() -> new ApplicationException(Errors.PUBLISHER_NOT_FOUND));
            specification.add(SearchCriteria.builder()
                    .key("user")
                    .value(user)
                    .operation(SearchOperation.EQUAL)
                    .build());
        }

        specification.add(SearchCriteria.builder()
                .key("name")
                .value(name)
                .operation(SearchOperation.MATCH)
                .build());

        return bookRepository.findAll(specification, pageable)
                .map(book -> mapper.map(book, BookResponseDto.class));
    }

    private void errorIfUserDoesNotHavePublisherAuthority() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null && auth.getAuthorities().stream().noneMatch(a -> a.getAuthority().equals(UserAuthority.ROLE_PUBLISHER.name()))) {
            throw new ApplicationException(Errors.USER_IS_NOT_PUBLISHER);
        }
    }
}
