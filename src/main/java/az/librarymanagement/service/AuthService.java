package az.librarymanagement.service;

import az.librarymanagement.dto.SignInDto;
import az.librarymanagement.dto.SignUpDto;

public interface AuthService {
    void signUp(SignUpDto dto);

    void signIn(SignInDto dto);
}
