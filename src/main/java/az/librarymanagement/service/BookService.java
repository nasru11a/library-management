package az.librarymanagement.service;

import az.librarymanagement.dto.BookRequestDto;
import az.librarymanagement.dto.BookResponseDto;
import az.librarymanagement.dto.BookUpdateDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface BookService {
    String publishNewBook(String username, BookRequestDto dto);

    List<BookResponseDto> listAllBooks();

    Page<BookResponseDto> search(String query, Long publisherId, Pageable pageable);

    BookResponseDto updateBook(Long bookId, BookUpdateDto dto, String username);
}
