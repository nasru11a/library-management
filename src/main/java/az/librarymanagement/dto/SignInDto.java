package az.librarymanagement.dto;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Data
@Builder
public class SignInDto {

    @Email
    @NotBlank(message = "Email should not be blank.")
    private String email;

    @NotBlank(message = "Password should not be empty.")
    private String password;
}
