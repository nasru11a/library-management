package az.librarymanagement.dto;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
@Builder
public class SignUpDto {

    @Email
    @NotBlank(message = "Email should not be blank.")
    private String email;

    @NotBlank(message = "Password should not be empty.")
    private String password;

    @NotBlank(message = "Firstname should not be blank.")
    private String firstName;

    private String lastName;

    @NotBlank(message = "Phone number is required.")
    private String phone;

    @NotNull
    private boolean iAmPublisher;

}
